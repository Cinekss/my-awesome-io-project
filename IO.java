/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io;

import javax.swing.JFrame;

/**
 *
 * @author marcinwojciechowski
 */
public class IO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
    
        JBtn bt = new JBtn();
        JText text = new JText();
        JFrame f = new JFrame();
        f.add(bt.createBtn1());
        f.add(bt.createBtn2());
        f.add(bt.createBtn3());
        f.add(text.createJText1());
        f.add(text.createJText2());
        f.add(text.createJText3());
        f.setSize(400,400);
        f.setLayout(null);  
        f.setVisible(true);  
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    
}
